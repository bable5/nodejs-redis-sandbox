FROM node:10.15.1-alpine as builder

WORKDIR /build
COPY ./ /build

RUN npm install --production

FROM node:10.15.1-alpine
LABEL maintainer="Sean L. Mooney <bable4@gmail.com>"

# Create a group and user to isolate the running process
RUN addgroup -S app && adduser -S -G app app && \
    mkdir -p /app
WORKDIR /app

COPY --from=builder /build/package.json /app
COPY --from=builder /build/node_modules /app/node_modules
COPY --from=builder /build/index.js index.js
COPY --from=builder /build/lib /app/lib
RUN chown -R app:app /app
USER app
EXPOSE 3000

CMD ["npm", "start"]
