const { promisify } = require('util');
const { adaptToPromises } = require('./redis-async');

const redis = require('redis');
const client = redis.createClient();
const asyncClient = adaptToPromises(client);

client.on("error", function (err) {
    console.log("Error " + err);
});

redis.Multi.prototype.execAsync = promisify(redis.Multi.prototype.exec);

client.multi()
    .sadd('bar', [1, 2, 3], redis.print)
    .expire('bar', 60)
    .execAsync()
    .then(v => {
        console.log(`After execAsync: ${v}`);
        return v;
    });

function saddWithExpire(key, value, ttl, cb) {
    client.multi()
        .sadd(key, value)
        .expire(key, ttl)
        .exec(cb);
}

const asyncSAddWithExpire = promisify(saddWithExpire)

const userKey = "user:1000:bas"

function describeSet(key, cb) {
    client.smembers(key, (err, reply) => {
        if (!err) {
            console.log(`'${key}' contains ${reply.length} members: ${reply}`)
        }

        cb(err, reply)
    });
}

async function redisTests() {
    const noop = () => { };
    describeSet(`c'nest pas un key`, noop);
    describeSet('baz', noop);
    describeSet('asyncBaz', noop);
    describeSet(userKey, noop);

    asyncClient.saddAddWithExpire('baz', [10, 11, 13, 13], 60).then((replies) => {
        console.log('After baz insert')
        console.log(`Err; ${err}, Replies: ${replies}`);
    });

    asyncSAddWithExpire('asyncBaz', ["KEY1", "KEY2", "KEY3"], 60)
        .then(res => {
            console.log(`After asyncBaz ${res}`)
        })

    client.smembers("foo", redis.print);
    client.hkeys("hash key", function (err, replies) {
        console.log(replies.length + " replies:");
        replies.forEach(function (reply, i) {
            console.log("    " + i + ": " + reply);
        });
    });

    client.multi()
        .sadd(userKey, ["VIEW_THING_1", "VIEW_THING_2", "VIEW_THING_3"])
        .expire(userKey, 300)
        .exec(function (err, replies) {
            console.log("MULTI got " + replies.length + " replies");
            replies.forEach(function (reply, index) {
                console.log("Reply " + index + ": " + reply.toString());
            });
        });

}

async function main() {
    try {
        await redisTests()
    } finally {
        client.quit()
    }
}

main();

// client.set("string key", "string val", (err, resp) => {
//     console.log(resp);
//     client.quit();
// });



// client.set("string key", "string val", redis.print);
// client.hset("hash key", "hashtest 1", "some value", redis.print);
// client.hset(["hash key", "hashtest 2", "some other value"], redis.print);
// client.smembers("foo", redis.print);
// client.smembers(userKey, (err, res) => {
//     console.log(`Current ${userKey} members: ${res}`)
// })
// client.ttl(userKey, (err, res) => {
//     console.log(`${userKey} ttl: ${res}`)
// })
// client.sadd("foo", [1, 2, 3], redis.print);
// client.sadd("foo", [1, 5, 7, 2, 3, 9], redis.print);
// client.expire('foo', 10, redis.print);





