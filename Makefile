DOCKER=docker

.PHONY: redis-up

redis-up:
	$(DOCKER) run -d --name node-redis-demo-redis -P redis:5-alpine