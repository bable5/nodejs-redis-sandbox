
const { promisify } = require('util');

function adaptToPromises(client) {
    return {
        get: promisify(client.get),
        set: promisify(client.set),
        smembers: promisify(client.smembers),
        sadd: promisify(client.sadd),
        saddAddWithExpire: promisify((key, value, ttl, cb) => {
            client.multi()
                .sadd(key, value)
                .expire(key, ttl)
                .exec()
        })
    }
}

module.exports = {
    adaptToPromises
};
